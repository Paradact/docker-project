from flask import Flask, request
import mysql.connector
import json

api = Flask(__name__)


def get_all():
    config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'variations_db'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM variations')
    # results = [{id: [chromosome, locus, alt, ref, frequency]} for (id, chromosome, locus, alt, ref, frequency) in cursor[]]
    cursor.close()
    connection.close()

    return cursor[0]


@api.route('/')
def index():
    return get_all()
    # chromosome = request.args.get('chr')
    # position   = request.args.get('pos')
    # nucleotide = request.args.get('nuc')

    # if not chromosome or not position or not nucleotide:
    #     return "Please specify chromosome, position, and nucleotide"

    return "Chromosome: {}\nPosition: {}\nNucleotide: {}".format(chromosome, position, nucleotide)


if __name__ == '__main__':
    api.run(host='0.0.0.0')
