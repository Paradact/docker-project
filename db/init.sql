CREATE DATABASE variations_db;
use variations_db;

CREATE TABLE variations (
  name VARCHAR(20),
  color VARCHAR(10)
);

INSERT INTO variations
  (name, color)
VALUES
  ('Lancelot', 'blue'),
  ('Galahad', 'yellow');

-- CREATE DATABASE variations_db;
-- use variations_db;
--
-- CREATE TABLE variations (
--   id int NOT NULL,
--   chromosome VARCHAR(2) NOT NULL,
--   locus VARCHAR(10) NOT NULL,
--   alt VARCHAR(50) NOT NULL,
--   ref VARCHAR(50) NOT NULL,
--   frequency float(10,9) NOT NULL,
--
--   PRIMARY KEY (ID)
-- );
--
--
-- INSERT INTO variations
--   (id, chromosome, locus, alt, ref, frequency)
-- VALUES
--   (781744002,'Y','2654979','C','G',2.97734e-05);
