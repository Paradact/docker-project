import os
import sys
import time
from vcf_parser import VCFParser
import mysql.connector

DATAPATH = "data/"
FILES = [f for f in os.listdir(DATAPATH) if f.endswith('.vcf')]# or f.endswith('.vcf.bgz')]
DBCONFIG = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'db'
    }

file = FILES[0]
# for file in FILES:
data = VCFParser(infile='data/'+file, split_variants=True, check_info=True)

def fill_db(id, chrom, pos, alt, ref, freq, tries=0):
    try:
        connection = mysql.connector.connect(**DBCONFIG)
    except (ConnectionRefusedError, mysql.connector.errors.InterfaceError) as e:
        if tries < 1000: #1000 = recursion limit
            time.sleep(5)
            return fill_db(id, chrom, pos, alt, ref, freq, tries+1)
        else:
            print('recursion limit reached')

    try:
        connection = mysql.connector.connect(**DBCONFIG)
        cursor = connection.cursor()
        cursor.execute('''INSERT INTO variations (id, chromosome, locus, alt, ref, frequency)
                          VALUES ({}, '{}', '{}', '{}', '{}', {})'''.format(int(id[2:]), chrom, pos, alt, ref, freq))
        cursor.close()
        connection.close()
    except ValueError as e:
        pass

count=0
for datum in data:
    while count < 100:
        try:
            id = datum['ID']
            chrom = datum['CHROM']
            pos = datum['POS']
            alt = datum['ALT']
            ref = datum['REF']
            freq = datum['info_dict']['AF'][0]

            print('('+str(id), chrom, pos, alt, ref, str(freq)+')', sep=',', end=',\n')
            # fill_db(id, chrom, pos, alt, ref, freq)

        except KeyError as e:
            pass

        count+=1
